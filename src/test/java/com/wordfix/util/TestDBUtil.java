package com.wordfix.util;

import org.junit.Assert;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.wordfix.constant.Constant;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestDBUtil {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Constant.getParameters();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test_001_getConnection() throws SQLException {
		
		Connection con = DBUtil.getConnection();
		
		Assert.assertNotNull(con);
		
		DBUtil.close(con);
		
		Assert.assertTrue(con.isClosed());
		
	}

}
