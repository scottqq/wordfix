package com.wordfix.constant;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wordfix.domain.DBConfig;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestConstant {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Constant.getParameters();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void test_001_logback() throws Exception {
				
		Logger logger = LoggerFactory.getLogger(TestConstant.class);
		
		logger.debug("This is a Test!");
		
		String logContent = FileUtils.readFileToString(new File("./logs/wordfix.log"));
		
		boolean condition = logContent.contains("This is a Test!");
		
		Assert.assertTrue(condition);
		
		FileUtils.deleteDirectory(new File("./logs"));
		
	}
	
	@Test
	public void test_002_loadDBConfig() {
		
		DBConfig dbConfig = Constant.DB_CONFIG;
		
		String expectedDbUrl = "jdbc:mysql://127.0.0.1/data_min?autoReconnect=true&useUnicode=true&characterEncoding=utf8";
		String expectedDbClass = "com.mysql.jdbc.Driver";
		String expectedDbUser = "root";
		String expectedDbPassword = "123456";
		
		Assert.assertEquals(expectedDbUrl, dbConfig.getDbUrl());
		Assert.assertEquals(expectedDbClass, dbConfig.getDbClass());
		Assert.assertEquals(expectedDbUser, dbConfig.getDbUser());
		Assert.assertEquals(expectedDbPassword, dbConfig.getDbPassword());
		
	}
	
}
