<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
String contextPath = request.getContextPath();
%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>暴走大预测</title>
		
		<script type="text/javascript" src="<%=contextPath%>/javascript/lib/jquery.js"></script>
		<style type="text/css">
		.s_ipt {
			width: 526px;
			height: 40px;
			font-size: 18px;
			font: arial;
			line-height: 22px;
			margin: 6px 0 0 7px;
			padding: 0;
			background: transparent;
			border: solid blue 1px;
			outline: 0;
		}
		.sbtn {
			border: solid black 1px;
			text-align: center;
			color:white;
			background-color:#00c;
			font-weight: bold;
			padding-top:12px;
			padding-bottom: 10px;
			padding-left:5px;
			padding-right:5px;
			height: 40px;
			cursor: pointer;
		}
		</style>
		<script type="text/javascript">
		$().ready(function() {
		
			$("img").click(function(){
				$("#name1").val("").focus();
				$("#resultDiv").html("");
				$("#transDiv").html("");
			});
			
			function dosubmit()
			{
				if($("#name1").val() == "")
				{
					$("#resultDiv").html("<h1>请输入单词</h1>");
					$("input[name=name]").focus();
					return false;
				}

				$.ajax({
					type: "post",
					url: "<%=contextPath%>/AjaxServlet",
					async: false,
					cache: false,
					data: { queryName: $("#name1").val(), type: "correct" },
					success: function(res){
						$("#cname").val(res);
						var ret = "<h1>您可能要找的单词是：<span style='color:red;'>"+res+"</span></h1>";
						$("#resultDiv").html(ret);
					}
				});
				
				$.ajax({
					type: "post",
					url: "<%=contextPath%>/AjaxServlet",
					async: true,
					cache: false,
					data: { queryName: $("#cname").val(), type: "translation" },
					beforeSend: function() {
						$("#transDiv").html("<h1>翻译中...</h1>");
						//$("#transDiv").html("<img src='img/animation-loading.gif' width='300' height='150'/>");
					},
					success: function(res){
						var ret = "<h1>翻译：<span style='font-size:20px;'>"+res+"</span></h1>";
						 $("#transDiv").html(ret);
					}
				});

			}
			
			
			
			$("input[name=name]").keyup(function(event){
				if(event.keyCode == 13){
					dosubmit();
				}
			});
			
			$("#correctBtn").click(function(){
				dosubmit();
			});
			
		});
		
		</script>
	</head>
<body>

	<div style="width: 50%; margin: 0 auto; clear: both;">
        <center><h2 style="font-size:50px;"><img src="img/baozou.png" width="300" height="150"/>纠正&翻译</h2></center>
        <center>
        <span>
            <input type="text" name="name" id="name1" class="s_ipt" autocomplete="off" />
            <input type="hidden" name="cname" id="cname"/>
        </span>
        <span style="left:0px;padding:0px;">
        	<span class="sbtn" id="correctBtn">预测+翻译</span>
        </span>
    	<div id="resultDiv" style="margin-top:5px; font-size:18px;"></div>
    	<div id="transDiv" style="margin-top:5px; font-size:18px;text-align:left;margin-left:100px;"></div>
    	</center>
	</div>
    
</body>
</html>