package com.wordfix.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wordfix.constant.Constant;
import com.wordfix.domain.DBConfig;

public class DBUtil 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(DBUtil.class);
	
	public static Connection getConnection()
	{
		try
		{
			DBConfig dbConfig = Constant.DB_CONFIG;
			
			Class.forName(dbConfig.getDbClass());
			return DriverManager.getConnection(dbConfig.getDbUrl(), dbConfig.getDbUser(), dbConfig.getDbPassword());
		}
		catch(Exception e)
		{
			LOGGER.error("error",e);
		}
		return null;
	}
	
	public static void close(Connection con)
	{
		try
		{
			if(con != null) con.close();
		}
		catch(Exception e){}
	}
	
	public static void close(Statement stmt)
	{
		try
		{
			if(stmt != null) stmt.close();
		}
		catch(Exception e){}
	}
	
	public static void close(PreparedStatement pst)
	{
		try
		{
			if(pst != null) pst.close();
		}
		catch(Exception e){}
	}
	
	public static void close(ResultSet rs)
	{
		try
		{
			if(rs != null) rs.close();
		}
		catch(Exception e){}
	}
	
	public static void close(Connection con, Statement stmt, ResultSet rs)
	{
		close(rs);
		close(stmt);
		close(con);
	}
	public static void close(Connection con, PreparedStatement pst, ResultSet rs)
	{
		close(rs);
		close(pst);
		close(con);
	}
}
