package com.wordfix.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class TransUtil 
{
	public static void main(String[] args) 
	{
		System.out.println(translate("hello"));
	}
	
	public static String simpleTrans(String input)
	{
		try
		{
			
			JSONObject obj = JSON.parseObject(translate(input));
			
			return obj.getString("translation") + "<br/>" + obj.getJSONObject("basic").getString("explains");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "";
	}
	
	public static String translate(String input)
	{
		String ret = "";
		try 
		{
			String strURL = " http://fanyi.youdao.com/openapi.do?keyfrom=kargocard&key=1719624492&type=data&doctype=json&version=1.1&q=" + URLEncoder.encode(input, "utf8");;
			ret = TransUtil.GetMsg(strURL);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}
	
	public static String PostMsg(String POST_URL, String strCotent) throws IOException 
	{
	
        URL postUrl = new URL(POST_URL);
        HttpURLConnection connection = (HttpURLConnection) postUrl.openConnection();
        System.setProperty("sun.net.client.defaultConnectTimeout", "3000");  
	    System.setProperty("sun.net.client.defaultReadTimeout", "3000"); 
	    connection.setConnectTimeout(3000);
	    connection.setReadTimeout(3000);
	    connection.setDoOutput(true);
	    connection.setDoInput(true);
	    connection.setRequestMethod("POST");
	    connection.setUseCaches(false);
	
	    connection.setInstanceFollowRedirects(true);
	    connection.setRequestProperty("Content-Type","application/json;charset=UTF-8");
	    
	    connection.connect();
	    
	    OutputStream out = connection.getOutputStream();
	    out.write(strCotent.getBytes("utf-8"));
	    out.close();
	           
	    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"utf-8"));//设置编码,否则中文乱码
	    String line="";
	    StringBuffer strBuf=new StringBuffer();
	    System.out.println("Post URL="+POST_URL);
	    while ((line = reader.readLine()) != null){
	    	strBuf.append(line);
	    }
	    reader.close();
	    System.out.println("responseCode=>" + connection.getResponseCode() + "| responseMessage=>" + connection.getResponseMessage());
	    connection.disconnect();
	    return strBuf.toString();
	}
	
	public static String GetMsg(String POST_URL) throws IOException{
	
	    URL postUrl = new URL(POST_URL);
	    HttpURLConnection connection = (HttpURLConnection) postUrl.openConnection();
	    System.setProperty("sun.net.client.defaultConnectTimeout", "3000");  
	    System.setProperty("sun.net.client.defaultReadTimeout", "3000"); 
	    connection.setConnectTimeout(3000);
	    connection.setReadTimeout(3000);
	    connection.setDoOutput(true);
	    connection.setDoInput(true);
	    connection.setRequestMethod("GET");
	    connection.setUseCaches(false);
	    connection.setInstanceFollowRedirects(true);
	    connection.setRequestProperty("Content-Type","application/json;charset=UTF-8");
	    connection.connect();
	    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"utf-8"));//设置编码,否则中文乱码
	    String line="";
	    StringBuffer strBuf=new StringBuffer();
	    System.out.println("Post URL="+POST_URL);
	    while ((line = reader.readLine()) != null){
	    	strBuf.append(line);
	    }
	    System.out.println("responseCode=>" + connection.getResponseCode() + "| responseMessage=>" + connection.getResponseMessage());
	    reader.close();
	    connection.disconnect();
	    return strBuf.toString();
	}

}
