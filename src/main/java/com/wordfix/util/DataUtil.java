package com.wordfix.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wordfix.domain.Word;

public class DataUtil 
{
	public static final String ALPH = "abcdefghijklmnopqrstuvwxyz";
	
	public static final char[] ALPH_ARRAY = ALPH.toCharArray();
	
	public static Logger logger = LoggerFactory.getLogger(DataUtil.class);
	
	/**
	 * 分词,将文章分成词数组
	 */
	public static List<String> words(String text)
	{
		List<String> wordList = new ArrayList<String>();
		Pattern p = Pattern.compile("[a-z]+",Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(text.toLowerCase());
		while(m.find())
		{
			wordList.add(m.group());
		}
		return wordList;
	}
	
	/**
	 * 训练输入的词语
	 */
	public static Map<String, Long> train(List<String> features)
	{
		Map<String, Long> map = new HashMap<String, Long>();
		
		Long count = 1L;
		
		for(String word : features)
		{
			count = map.get(word);
			if(count == null)
			{
				count = 1L;
			}
			else
			{
				count++;
			}
			map.put(word, count);
		}
		return map;
	}
	
	/**
	 * splits：将word依次按照每一位分割成前后两半。比如，'abc'会被分割成 [('', 'abc'), ('a', 'bc'), ('ab', 'c'), ('abc', '')]
	 */
	public static List<Word> splits(String input)
	{
		List<Word> wordList = new ArrayList<Word>();
		
		wordList.add(new Word("",input));
		for(int i = 0; i < input.length(); i++)
		{
			Word word = new Word();
			word.setFirst(input.substring(0, i+1));
			word.setSecond(input.substring(i+1));
			wordList.add(word);
		}
		//wordList.add(new Word(input,""));
		return wordList;
	}
	
	/**
	 * deletes：依次删除word的每一位后、所形成的所有新词。比如，'abc'对应的deletes就是 ['bc', 'ac', 'ab'] 。
	 */
	public static List<String> deletes(String input)
	{
		List<String> wordList = new ArrayList<String>();
		
		String e;
		for(int i = 0; i < input.length(); i++)
		{
			e = input.substring(0,i) + input.substring(i+1);
			wordList.add(e);
		}
		
		return wordList;
	}
	
	/**
	 * transposes：依次交换word的邻近两位，所形成的所有新词。比如，'abc'对应的transposes就是 ['bac', 'acb'] 
	 */
	public static List<String> transposes(String input)
	{
		List<String> wordList = new ArrayList<String>();
		char[] inputArray = input.toCharArray();
		
		int len = inputArray.length;
		
		char[] newArray;
		char temp;
		for(int i = 0; i < len-1; i++)
		{
			newArray = Arrays.copyOf(inputArray, len);
			temp = newArray[i];
			newArray[i] = newArray[i+1];
			newArray[i+1] = temp;
			wordList.add(String.valueOf(newArray));
		}
		
		return wordList;
	}
	
	/**
	 * replaces：将word的每一位依次替换成其他25个字母，所形成的所有新词。比如，'abc'对应的replaces就是 ['abc', 'bbc', 'cbc', ... , 'abx', ' aby', 'abz' ] ，一共包含78个词（26 × 3）
	 */
	public static List<String> replaces(String input)
	{
		List<String> wordList = new ArrayList<String>();
		
		char[] inputCharArray = input.toCharArray();
		
		int len = inputCharArray.length;
		
		for(char curChar : ALPH_ARRAY)
		{
			for(int j = 0; j < len; j++)
			{
				char[] newArray = Arrays.copyOf(inputCharArray, len);
				newArray[j] = curChar;
				wordList.add(String.valueOf(newArray));
			}
		}
		
		return wordList;
	}
	
	/**
	 * inserts：在word的邻近两位之间依次插入一个字母，所形成的所有新词。比如，'abc' 对应的inserts就是['aabc', 'babc', 'cabc', ..., 'abcx', 'abcy', 'abcz']，一共包含104个词（26 × 4）
	 */
	public static List<String> inserts(String input)
	{
		List<String> wordList = new ArrayList<String>();
		
		List<Word> words = splits(input);
		
		String e;
		for(char curChar : ALPH_ARRAY)
		{
			for(Word word : words)
			{
				e = word.getFirst() + curChar + word.getSecond();
				wordList.add(e);
			}
		}
		
		return wordList;
	}
	
	public static Set<String> edit1(String input)
	{
		Set<String> wordSet = new HashSet<String>();
		
		wordSet.addAll(deletes(input));
		wordSet.addAll(transposes(input));
		wordSet.addAll(replaces(input));
		wordSet.addAll(inserts(input));
		return wordSet;
	}
	
	public static Set<String> edit2(String input)
	{
		Set<String> wordSet = new HashSet<String>();
		
		Set<String> wordSet1 = edit1(input);
		
		for(String e : wordSet1)
		{
			wordSet.addAll(edit1(e));
		}
		
		return wordSet;
	}
	
	public static Set<String> known(String input)
	{
		Set<String> knownSet = new HashSet<String>();
		
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			con = DBUtil.getConnection();
			
			stmt = con.createStatement();
			
			rs = stmt.executeQuery("select `word` from word_count where word='"+input+"' order by `count` desc limit 1");
			if(rs.next())
			{
				knownSet.add(rs.getString("word"));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		finally
		{
			DBUtil.close(con, stmt, rs);
		}
		return knownSet;
	}
	
	public static Set<String> known(Set<String> wordSet)
	{
		Set<String> knownSet = new HashSet<String>();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			con = DBUtil.getConnection();
			
			stmt = con.createStatement();
			
			StringBuilder sqlBuf = new StringBuilder();
			sqlBuf.append("select `word` from word_count where 1=1 ");
			if(wordSet != null && !wordSet.isEmpty())
			{
				sqlBuf.append(" and word in (");
				int i = 0;
				for(String word : wordSet)
				{
					if(i >= 1)
					{
						sqlBuf.append(",");
					}
					sqlBuf.append("'").append(StrUtil.filter(word, "'")).append("'");
					i++;
				}
				sqlBuf.append(") ");
			}
			String sql = sqlBuf.toString();
			logger.debug(sql);
			rs = stmt.executeQuery(sql);
			while(rs.next())
			{
				knownSet.add(rs.getString("word"));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		finally
		{
			DBUtil.close(con, stmt, rs);
		}
		return knownSet;
	}
	
	public static Set<String> know_edit2(String input)
	{
		Set<String> wordSet = edit2(input);
		
		return known(wordSet);
	}
	
	
}
