package com.wordfix.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wordfix.constant.Constant;

public class AppContextListener implements ServletContextListener 
{
	
	private static Logger LOGGER;

	@Override
	public void contextDestroyed(ServletContextEvent event) 
	{
		LOGGER.info("Web Container shutdown.........");
	}

	@Override
	public void contextInitialized(ServletContextEvent event) 
	{
		Constant.getParameters();
		LOGGER = LoggerFactory.getLogger(AppContextListener.class);
		LOGGER.info("Web Container startup.........");
	}

}
