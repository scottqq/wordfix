package com.wordfix.domain;

import java.util.ArrayList;
import java.util.List;

public class Response 
{
	private List<String> suggestions = new ArrayList<String>();
	
	public List<String> getSuggestions() {
		return suggestions;
	}
	public void setSuggestions(List<String> suggestions) {
		this.suggestions = suggestions;
	}
}
