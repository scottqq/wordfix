package com.wordfix.domain;

import java.io.Serializable;

public class Word implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String first;
	private String second;
	
	public Word() {}
	
	public Word(String first, String second)
	{
		this.first = first;
		this.second = second;
	}
	
	public String getFirst() 
	{
		return first;
	}
	public void setFirst(String first) 
	{
		this.first = first;
	}
	public String getSecond() 
	{
		return second;
	}
	public void setSecond(String second) 
	{
		this.second = second;
	}
	
	@Override
	public String toString() 
	{
		return "('"+this.first+"', '"+this.second+"')";
	}
}
