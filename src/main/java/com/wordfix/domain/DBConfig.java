package com.wordfix.domain;

public class DBConfig 
{
	private String dbUrl;
	private String dbClass;
	private String dbUser;
	private String dbPassword;
	
	public DBConfig(String dbUrl, String dbClass, String dbUser, String dbPassword)
	{
		this.dbUrl = dbUrl;
		this.dbClass = dbClass;
		this.dbUser = dbUser;
		this.dbPassword = dbPassword;
	}

	public String getDbUrl() {
		return dbUrl;
	}

	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}

	public String getDbClass() {
		return dbClass;
	}

	public void setDbClass(String dbClass) {
		this.dbClass = dbClass;
	}

	public String getDbUser() {
		return dbUser;
	}

	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}
	
	
}
