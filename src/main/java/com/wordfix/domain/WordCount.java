package com.wordfix.domain;

public class WordCount
{
	private String word;
	private Long count = 0L;
	
	public String getWord() 
	{
		return word;
	}
	
	public void setWord(String word) 
	{
		this.word = word;
	}
	
	public Long getCount() 
	{
		return count;
	}
	
	public void setCount(Long count) 
	{
		this.count = count;
	}
	
}
