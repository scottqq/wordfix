package com.wordfix.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.wordfix.dao.WordDao;
import com.wordfix.domain.Response;
import com.wordfix.util.DataUtil;

public class WordService 
{

	
	public static Logger logger = LoggerFactory.getLogger(WordService.class);
	
	public static String correct(String input)
	{
		List<String> candidates = new ArrayList<String>();
		
		candidates.addAll(DataUtil.known(input));
		
		if(candidates.isEmpty())
		{
			candidates.addAll(DataUtil.known(DataUtil.edit1(input)));
		}
		
		if(candidates.isEmpty())
		{
			candidates.addAll(DataUtil.know_edit2(input));
		}
		
		if(candidates.isEmpty())
		{
			candidates.add(input);
		}
		
		List<String> result = WordDao.getProbabilityWords(candidates, 1);
		
		
		
		if(result != null && !result.isEmpty())
		{
			return result.get(0);
		}
		return input;
	}
	
	
	public static List<String> autoComplete(String input)
	{
		return WordDao.getAutoCompleteWord(input, 10);
	}
	
	public static String autJsonComplete(String input)
	{
		Response response = new Response();
		response.setSuggestions(autoComplete(input));
		
		String result = JSON.toJSONString(response);
				
		result = result.replaceFirst("\"suggestions\":", "");
		result = result.replaceAll("[\\{|\\}]", "");
		
		logger.info(result);
		
		return result;
	}

}
