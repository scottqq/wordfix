package com.wordfix.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.wordfix.util.DBUtil;
import com.wordfix.util.StrUtil;

public class WordDao 
{
	public static Logger logger = LoggerFactory.getLogger(WordDao.class);
	
	public static List<String> getAutoCompleteWord(String word, int num)
	{
		List<String> wordList = new ArrayList<String>();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			con = DBUtil.getConnection();
			stmt = con.createStatement();
			StringBuilder sqlBuf = new StringBuilder();
			sqlBuf.append("select `word` from `word_count` where `word` like '").append(word).append("%' order by `count` desc limit ").append(num);
			String sql = sqlBuf.toString();
			logger.debug(sql);
			rs = stmt.executeQuery(sql);
			while(rs.next())
			{
				wordList.add(rs.getString("word"));
			}
		}
		catch(Exception e)
		{
			logger.error("error",e);
		}
		finally
		{
			DBUtil.close(con, stmt, rs);
		}
		return wordList;
	}
	
	
	public static List<String> getProbabilityWords(List<String> words, int num)
	{
		List<String> wordList = new ArrayList<String>();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			con = DBUtil.getConnection();
			stmt = con.createStatement();
			StringBuilder sqlBuf = new StringBuilder();
			
			sqlBuf.append("select `word` from `word_count` where 1=1 ");
			if(words != null && !words.isEmpty())
			{
				sqlBuf.append(" and `word` in (");
				for(int i = 0; i < words.size(); i++)
				{
					if(i >= 1)
					{
						sqlBuf.append(",");
					}
					sqlBuf.append("'").append(StrUtil.filter(words.get(i),"'")).append("'");
				}
				sqlBuf.append(") ");
			}
			
			sqlBuf.append(" order by `count` desc limit ").append(num);
			
			String sql = sqlBuf.toString();
			logger.debug(sql);
			rs = stmt.executeQuery(sql);
			while(rs.next())
			{
				wordList.add(rs.getString("word"));
			}
		}
		catch(Exception e)
		{
			logger.error("error",e);
		}
		finally
		{
			DBUtil.close(con, stmt, rs);
		}
		return wordList;
	}
	

}
