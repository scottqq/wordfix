package com.wordfix.constant;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

import com.wordfix.domain.DBConfig;

public class Constant 
{
	
	public static DBConfig DB_CONFIG;
	
	public static void getParameters()
	{
		loadLogBackConfig();
		loadDBConfig();
	}
	
	
	private static void loadDBConfig()
	{
		try 
		{
			PropertiesConfiguration conf = new PropertiesConfiguration(Constant.class.getClassLoader().getResource("dbconfig.properties"));
			String dbUrl = conf.getString("jdbc.driver-url");
			String dbClass = conf.getString("jdbc.driver-class");
			String dbUser = conf.getString("jdbc.user");
			String dbPassword = conf.getString("jdbc.password");
			DB_CONFIG = new DBConfig(dbUrl,dbClass,dbUser,dbPassword);
		} 
		catch (ConfigurationException e) {
			System.err.println(e);
		}
	}
	
	private static void loadLogBackConfig() 
	{
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
		JoranConfigurator configurator = new JoranConfigurator();
		configurator.setContext(lc);
		lc.reset();
		try {
			configurator.doConfigure(Constant.class.getClassLoader().getResource("logback.xml"));
		} catch (JoranException e) {
			
		}
	}
	
}
