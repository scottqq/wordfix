package com.wordfix.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wordfix.service.WordService;
import com.wordfix.util.TransUtil;


/**
 * Servlet implementation class AjaxServlet
 */
public class AjaxServlet extends HttpServlet 
{
	private static Logger logger = LoggerFactory.getLogger(AjaxServlet.class);
	private static final long serialVersionUID = 1L;
       
    public AjaxServlet() 
    {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String param = request.getParameter("queryName");
		String type = request.getParameter("type");
		logger.info("queryName="+param);
		logger.info("type="+type);
		PrintWriter pw = response.getWriter();
		if("autocomplete".equals(type))
		{
			pw.append(WordService.autJsonComplete(param));
		}
		else if("correct".equals(type))
		{
			pw.append(WordService.correct(param));
		}
		else if("translation".equals(type))
		{
			//String p = WordService.correct(param);
			String r = TransUtil.simpleTrans(param);
			logger.info("trans : " + r);
			r = r.replaceAll("[\\[|\\]|\"]", "");
			r = r.replaceAll(",", "<br/>");
			if("".equals(r))
			{
				//r = "该词汇超出了词库的范围";
				r = "\u8be5\u8bcd\u6c47\u8d85\u51fa\u4e86\u8bcd\u5e93\u7684\u8303\u56f4";
			}
			logger.info("relapce : " + r);
			pw.append(r);
		}
		pw.flush();
		pw.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		doGet(request,response);
	}

}
