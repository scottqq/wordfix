本项目是基于贝叶斯推断的统计方法实现的一个单词拼写并且翻译的一个样列

项目的具体实现细节可以参看document下的docx文件

运用mapreduce的思想，基于maven和mysql实现

数据库的脚本下载：http://pan.baidu.com/s/1eQLqtAM

部署方法：
1 下载数据库脚本在本地的mysql上执行
2 git clone 到本地以后，cd到项目的目录下面运行，根据您的情况修改src/main/resources/dbconfig.properties
3 运行mvn jetty:run
4 在浏览器中访问http://127.0.0.1:8080/wordfix/index.jsp

tomcat下的部署方法
1 下载数据库脚本在本地的mysql上执行
2 git clone 到本地以后，cd到项目的目录下面运行，根据您的情况修改src/main/resources/dbconfig.properties
3 运行mvn package -DskipTests,将target下的wordfix.war拷贝到tomcat的webapp目录下,运行tomcat/bin/startup.sh
4 在浏览器中访问http://127.0.0.1:8080/wordfix/index.jsp

有啥问题或者建议可以联系邮箱: duzhengxing625@163.com

